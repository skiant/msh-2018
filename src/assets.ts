/* AUTO GENERATED FILE. DO NOT MODIFY. YOU WILL LOSE YOUR CHANGES ON BUILD. */

export namespace Images {
    export class ImagesBackgroundTemplate {
        static getName(): string { return 'background_template'; }

        static getPNG(): string { return require('assets/images/background_template.png'); }
    }
    export class ImagesBall {
        static getName(): string { return 'ball'; }

        static getPNG(): string { return require('assets/images/ball.png'); }
    }
    export class ImagesCar {
        static getName(): string { return 'car'; }

        static getPNG(): string { return require('assets/images/car.png'); }
    }
    export class ImagesGoal {
        static getName(): string { return 'goal'; }

        static getPNG(): string { return require('assets/images/goal.png'); }
    }
    export class ImagesTerrain {
        static getName(): string { return 'terrain'; }

        static getPNG(): string { return require('assets/images/terrain.png'); }
    }
}

export namespace Spritesheets {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace Atlases {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace Audio {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace Audiosprites {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace GoogleWebFonts {
    export const Barrio: string = 'Barrio';
}

export namespace CustomWebFonts {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace BitmapFonts {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace JSON {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace XML {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace Text {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace Scripts {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}
export namespace Shaders {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}
export namespace Misc {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}
