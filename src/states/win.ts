import { App } from '../app';

export default class Win extends Phaser.State {
	private message: Phaser.Text;
	private scoreMessage: Phaser.Text;

	public create() {
		const finalScore = (<App>this.game).global.score;

		const style = { font: '48px serif', fill: '#fff', align: 'center' };
		this.message = this.game.add.text(this.game.world.centerX, this.game.world.centerY, `Congrats! Final score: ${finalScore}`, style );
		this.message.anchor.set(0.5);
	}

	public update() {
	}
}
