
export default class GameOver extends Phaser.State {
	private message: Phaser.Text;
	private startMessage: Phaser.Text;

	public create() {
		this.game.stage.backgroundColor = '#4a9233';

		const style = { font: '48px serif', fill: '#7d0e0e', align: 'center' };
		this.message = this.game.add.text(this.game.world.centerX, this.game.world.centerY, `Dark Rocket: Souls League`, style );
		this.message.anchor.set(0.5);


		const startStyle = { font: '30px sans-serif', fill: '#fff', align: 'center' };
		this.startMessage = this.game.add.text(this.game.world.centerX, this.game.world.height - 100, `Press space to start`, startStyle);
		this.startMessage.anchor.set(0.5);
	}

	public update() {
		if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
			this.game.state.start('level');
		}
	}
}
