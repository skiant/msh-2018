import * as Assets from '../assets';
import * as AssetUtils from '../utils/assetUtils';
import { App } from '../app';

export default class Scene extends Phaser.State {
	private carSprite: Phaser.Sprite;
	private goalSprite: Phaser.Sprite;
	private ballSprite: Phaser.Sprite;
	private background: Phaser.Sprite;
	private cursors: any;
	private statusText: Phaser.Text;

	private levelTimer = 15;
	private timeLeft: number;

	private carHandling = 50;
	private carAccel = 500;
	private carInertia = .6;
	private carMass = 5;

	private ballMass = 1;
	private ballInertia = .2;

	public preload(): void {
		this.game.physics.startSystem(Phaser.Physics.P2JS);
		this.game.physics.p2.restitution = 0.9;
		this.cursors = this.game.input.keyboard.createCursorKeys();
		AssetUtils.Loader.loadAllAssets(this.game);
	}

	public create(): void {
		const style = { font: '48px sans-serif', fill: '#fff', align: 'center' };
		this.statusText = this.game.add.text(this.game.world.centerX, 50, `Press space to start`, style);
		this.statusText.anchor.set(0.5);

		// Background
		this.background = this.game.add.sprite(0, 0, Assets.Images.ImagesTerrain.getName());
		this.background.alpha = 0.4;

		// Init the car
		this.carSprite = this.game.add.sprite(100, this.game.world.centerY + this.generateRandomInteger(-100, 100), Assets.Images.ImagesCar.getName());
		this.game.physics.p2.enable(this.carSprite);
		this.carSprite.body.collideWorldBounds = true;
		this.carSprite.body.mass = 1;
		this.carSprite.body.damping = this.carInertia;
		this.carSprite.body.angle = 90 + this.generateRandomInteger(-10, 10);

		// Init the goal
		this.goalSprite = this.game.add.sprite(this.game.world.width - 20, this.game.world.centerY, Assets.Images.ImagesGoal.getName());
		this.game.physics.p2.enable(this.goalSprite);
		this.goalSprite.body.kinematic = true;

		// Init the ball
		this.ballSprite = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, Assets.Images.ImagesBall.getName());
		this.game.physics.p2.enable(this.ballSprite);
		this.ballSprite.body.collideWorldBounds = true;
		this.ballSprite.body.setCircle(7);
		this.ballSprite.body.mass = .5;
		this.ballSprite.body.damping = this.ballInertia;

		this.ballSprite.body.onBeginContact.add(this.goalHit, this);

		this.startGame();
	}

	public update() {
		if (this.timeLeft <= 0) {
			(<App>this.game).global.score += 30;
			this.game.state.start('gameover');
		}

		if (this.cursors.left.isDown) {
			this.carSprite.body.rotateLeft(this.carHandling);
		} else if (this.cursors.right.isDown) {
			this.carSprite.body.rotateRight(this.carHandling);
		} else {
			this.carSprite.body.setZeroRotation();
		}

		if (this.cursors.up.isDown) {
			this.carSprite.body.thrust(this.carAccel);
		}

		if (this.cursors.down.isDown) {
			this.carSprite.body.reverse(this.carAccel);
		}
	}

	private startGame() {
		this.timeLeft = this.levelTimer;
		this.statusText.text = `${this.timeLeft}`;

		this.game.time.events.repeat(Phaser.Timer.SECOND, this.levelTimer, () => {
			this.timeLeft--;
			this.statusText.text = `${this.timeLeft}`;
		});
	}

	private goalHit(body: Phaser.Physics.P2.Body | null) {
		if (body && body.sprite.key === 'goal') {
			(<App>this.game).global.score += (this.levelTimer - this.timeLeft);
			this.game.state.start('win');
		}
	}

	private generateRandomInteger(min: number, max: number) {
		return Math.floor(min + Math.random() * (max + 1 - min));
	}
}
