import { App } from '../app';

export default class GameOver extends Phaser.State {
	private message: Phaser.Text;
	private retryMessage: Phaser.Text;

	public create() {
		const style = { font: '60px serif', fill: '#7d0e0e', align: 'center' };
		this.message = this.game.add.text(this.game.world.centerX, this.game.world.centerY, `YOU DIED`, style );
		this.message.anchor.set(0.5);

		const restartStyle = { font: '30px sans-serif', fill: '#fff', align: 'center' };
		this.retryMessage = this.game.add.text(this.game.world.centerX, this.game.world.height - 100, `Score: ${(<App>this.game).global.score} - Press space to retry`, restartStyle);
		this.retryMessage.anchor.set(0.5);
	}

	public update() {
		if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
			this.game.state.start('level');
		}
	}
}
