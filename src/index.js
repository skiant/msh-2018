import { Game, WEBGL } from 'phaser-ce';

const game = new Game(800, 600, WEBGL, '', { create, update, preload});

function preload() {
  game.load.image('car', 'assets/car.gif');
}

function create() {
  game.add.sprite(0, 0, 'car')
}

function update() {

}
