import 'p2';
import 'pixi';
import 'phaser';

import * as WebFontLoader from 'webfontloader';

import Level from './states/level';
import Gameover from './states/gameover';
import Boot from './states/boot';
import Win from './states/win';
import * as Utils from './utils/utils';
import * as Assets from './assets';

export class App extends Phaser.Game {
	global: {
		score: number;
	};

	constructor(config: Phaser.IGameConfig) {
		super (config);

		this.global = {
			score: 0,
		};

		this.state.add('boot', Boot);
		this.state.add('level', Level);
		this.state.add('gameover', Gameover);
		this.state.add('win', Win);
		this.state.start('boot');
	}
}

function startApp(): void {
	let gameWidth: number = DEFAULT_GAME_WIDTH;
	let gameHeight: number = DEFAULT_GAME_HEIGHT;

	if (SCALE_MODE === 'USER_SCALE') {
		let screenMetrics: Utils.ScreenMetrics = Utils.ScreenUtils.calculateScreenMetrics(gameWidth, gameHeight);

		gameWidth = screenMetrics.gameWidth;
		gameHeight = screenMetrics.gameHeight;
	}

	// There are a few more options you can set if needed, just take a look at Phaser.IGameConfig
	let gameConfig: Phaser.IGameConfig = {
		width: gameWidth,
		height: gameHeight,
		renderer: Phaser.AUTO,
		parent: '',
		resolution: 1
	};

	let app = new App(gameConfig);
}

window.onload = () => {
	let webFontLoaderOptions: any = null;
	let webFontsToLoad: string[] = GOOGLE_WEB_FONTS;

	if (webFontsToLoad.length > 0) {
		webFontLoaderOptions = (webFontLoaderOptions || {});

		webFontLoaderOptions.google = {
			families: webFontsToLoad
		};
	}

	if (webFontLoaderOptions === null) {
		// Just start the game, we don't need any additional fonts
		startApp();
	} else {
		// Load the fonts defined in webFontsToLoad from Google Web Fonts, and/or any Local Fonts then start the game knowing the fonts are available
		webFontLoaderOptions.active = startApp;

		WebFontLoader.load(webFontLoaderOptions);
	}
};
